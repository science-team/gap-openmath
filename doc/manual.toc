\contentsline {chapter}{\numberline {1}\textcolor {Chapter }{Introduction and installation}}{4}{chapter.1}%
\contentsline {section}{\numberline {1.1}\textcolor {Chapter }{Brief description of the package}}{4}{section.1.1}%
\contentsline {section}{\numberline {1.2}\textcolor {Chapter }{Installation of the package}}{4}{section.1.2}%
\contentsline {chapter}{\numberline {2}\textcolor {Chapter }{\textsf {OpenMath} functionality in \textsf {GAP}}}{6}{chapter.2}%
\contentsline {section}{\numberline {2.1}\textcolor {Chapter }{Viewing \textsf {OpenMath} representation of an object}}{6}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}\textcolor {Chapter }{OMPrint}}{6}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}\textcolor {Chapter }{OMString}}{11}{subsection.2.1.2}%
\contentsline {section}{\numberline {2.2}\textcolor {Chapter }{Reading \textsf {OpenMath} code from streams and strings}}{11}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}\textcolor {Chapter }{OMGetObject}}{11}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}\textcolor {Chapter }{EvalOMString}}{12}{subsection.2.2.2}%
\contentsline {section}{\numberline {2.3}\textcolor {Chapter }{Writing \textsf {OpenMath} code to streams}}{12}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}\textcolor {Chapter }{IsOpenMathWriter}}{12}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}\textcolor {Chapter }{OpenMathXMLWriter}}{12}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {2.3.3}\textcolor {Chapter }{OpenMathBinaryWriter}}{13}{subsection.2.3.3}%
\contentsline {subsection}{\numberline {2.3.4}\textcolor {Chapter }{OMPutObject}}{13}{subsection.2.3.4}%
\contentsline {subsection}{\numberline {2.3.5}\textcolor {Chapter }{OMPlainString}}{13}{subsection.2.3.5}%
\contentsline {section}{\numberline {2.4}\textcolor {Chapter }{Utilities}}{14}{section.2.4}%
\contentsline {subsection}{\numberline {2.4.1}\textcolor {Chapter }{OMTestXML}}{14}{subsection.2.4.1}%
\contentsline {subsection}{\numberline {2.4.2}\textcolor {Chapter }{OMTestBinary}}{14}{subsection.2.4.2}%
\contentsline {chapter}{\numberline {3}\textcolor {Chapter }{Extending the \textsf {OpenMath} package}}{15}{chapter.3}%
\contentsline {section}{\numberline {3.1}\textcolor {Chapter }{Exploring the range of supported symbols}}{15}{section.3.1}%
\contentsline {section}{\numberline {3.2}\textcolor {Chapter }{Adding support for private content dictionaries and symbols}}{16}{section.3.2}%
\contentsline {chapter}{References}{18}{chapter*.5}%
\contentsline {chapter}{Index}{19}{section*.6}%
